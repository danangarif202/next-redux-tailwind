import React, { Component } from "react";
import { withRouter } from "next/router";
import { connect } from "react-redux";

class Card extends Component {
    state = {}
    render() {
        const { home } = this.props;
        return (
            <>
                {
                    home.dataCard.data != undefined &&
                    <div className="p-6 max-w-sm bg-white rounded-lg border border-gray-200 shadow-md">
                        <img
                            className="w-full"
                            src={`https://api-rfda.herokuapp.com/` + home.dataCard.data.image}
                            alt="gambar product"
                        />
                        <div>
                            <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900">{home.dataCard.data.title}</h5>
                        </div>
                        <p class="mb-3 font-normal text-gray-700 dark:text-gray-400">Here are the biggest enterprise technology acquisitions of 2021 so far, in reverse chronological order.</p>
                        <div className="inline-flex items-center py-2 px-3 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800">
                            Read more
                        </div>
                    </div>
                }
            </>
        );
    }
}

const mapState = (state) => ({
    home: state.home,
});

export default withRouter(connect(mapState)(Card));
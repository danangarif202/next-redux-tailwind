import axios from "axios";

const config = {
  path: "https://api-rfda.herokuapp.com/products/",
};

const homeApi = {
  async browse(id) {
    try {
      const resp = await axios.get(config.path + id);
      return resp.data;
    } catch (error) {
      throw error;
    }
  },
};

export default homeApi;

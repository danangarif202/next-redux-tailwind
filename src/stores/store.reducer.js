import { combineReducers } from "redux";
import homeStore from "./home/home.store";

export default combineReducers({
    home: homeStore,
});


const storePlugin = {
  async action(callback, dispatch, onError = () => { }) {
    try {
      await callback();
    } catch (error) {
      onError(error);
    }
  },
};

export default storePlugin;

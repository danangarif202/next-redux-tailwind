import Head from 'next/head'
import React, { Component } from "react";
import { withRouter } from "next/router";
import { connect } from "react-redux";
import homeAction from "../src/stores/home/home.action";
import styles from '../src/styles/Home.module.css'
import Card from '../src/components/Card'


class Home extends Component {
  constructor(props) {
    super(props);
  }
  state = {}

  componentDidMount() {
    const { getHome } = this.props;
    getHome("128", (status) => { });
  }

  render() {
    return (
      <div className={styles.container}>
        <Head>
          <title>Create Next App</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>

        <main className={styles.main}>
          <h1 className={styles.title}>
            Welcome to <a href="https://nextjs.org">Next.js!</a>
          </h1>

          <p className={styles.description + ' mb-6'}>
            Get started by editing{' '}
            <code className={styles.code}>pages/index.js</code>
          </p>
          <Card />
        </main>

        <footer className={styles.footer}>
          <div className='flex items-center'>
            Powered by{' '}
            <img src="/vercel.svg" alt="Vercel Logo" className={styles.logo} />
          </div>
        </footer>
      </div>
    );
  }
}

const mapState = (state) => ({});
const mapDispatch = {
  getHome: homeAction.browse,
};

export default withRouter(connect(mapState, mapDispatch)(Home));


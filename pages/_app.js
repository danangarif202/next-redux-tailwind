import "tailwindcss/tailwind.css";
import "../src/styles/globals.css";
import { Provider } from "react-redux";
import configureAppStore from "../src/stores/store";
import { Component } from "react";
import { withRouter } from "next/router";

const store = configureAppStore();

class MyApp extends Component {
  componentDidMount() { }
  componentWillUnmount() { }

  render() {
    const { Component, pageProps } = this.props;
    return (
      <>
        <Provider store={store}>
          <Component {...pageProps} />
        </Provider>
      </>
    );
  }
}

export default withRouter(MyApp);
